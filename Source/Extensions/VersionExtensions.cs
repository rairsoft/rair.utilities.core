﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rair.Utilities.Core.Extensions
{
    public static class VersionExtensions
    {
        /// <summary>
        /// Returns full version string. i.e. x.x.x.x
        /// </summary>
        public static string Describe(this Version ver, bool incluedRev = false)
        {
            if (ver == null) return string.Empty;

            var desc = $"{ver.Major}.{ver.Minor}.{ver.Build}";

            if (incluedRev) desc += $".{ver.Revision}";

            return desc;
        }
    }
}
