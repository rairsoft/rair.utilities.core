﻿namespace Rair.Utilities.Core.Extensions
{
    public static class ObjectExtensions
    {
        public static T To<T>(this object obj)
        {
            if(obj == null) { return default(T); }
            return (T)obj;
        }
    }
}