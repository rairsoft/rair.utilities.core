﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rair.Utilities.Core.Extensions
{
    public static class CollectionExtensions
    {
        public static bool IsEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null) { return true; }

            var collection = enumerable as ICollection<T>;
            if (collection != null) { return collection.Count < 1; }
            return !enumerable.Any();
        }

        public static bool HasContent<T>(this IEnumerable<T> enumeration)
        {
            return !enumeration.IsEmpty();
        }

        public static void ForEach<T>(this IEnumerable<T> list, Action<T> action)
        {
            if (list == null) { throw new Exception(); }

            foreach (var item in list)
            {
                action(item);
            }
        }

        public static void MoveUp<T>(this IEnumerable<T> items, T item)
        {
            var enumerable = items as IList<T> ?? items.ToList();
            if (enumerable.Count <= 1) return;

            var itemIndex = enumerable.IndexOf(item);
            if (itemIndex == 0) return;

            var temp = enumerable[itemIndex - 1];
            enumerable[itemIndex - 1] = enumerable[itemIndex];
            enumerable[itemIndex] = temp;
        }

        public static void MoveDown<T>(this IEnumerable<T> items, T item)
        {
            var enumerable = items as IList<T> ?? items.ToList();
            if (enumerable.Count <= 1) return;

            var itemIndex = enumerable.IndexOf(item);
            if (itemIndex + 1 == enumerable.Count) return;

            var temp = enumerable[itemIndex + 1];
            enumerable[itemIndex + 1] = enumerable[itemIndex];
            enumerable[itemIndex] = temp;
        }

        public static void SetValues<T>(this IList<T> list, IEnumerable<T> values, bool clearList = true)
        {
            if (list == null) return;
            if (values == null) return;
            var enumerable = values as IList<T> ?? values.ToList();
            if (clearList) list.Clear();
            enumerable.ForEach(list.Add);
        }
    }
}