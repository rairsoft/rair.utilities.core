﻿using System;
using System.Text;

namespace Rair.Utilities.Core.Extensions
{
    public static class StringBuilderExtensions
    {
        public static string RemoveLastIndexOf(this StringBuilder stringBuilder, string character)
        {
            var str = stringBuilder.ToString();
            var lastIdx = str.LastIndexOf(",", StringComparison.Ordinal);
            return str.Substring(0, lastIdx);
        }
    }
}
