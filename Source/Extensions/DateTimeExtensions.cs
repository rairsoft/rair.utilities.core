﻿using System;

namespace Rair.Utilities.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToMySQLFormat(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd HH:mm:ss");
        }
        public static string ToMySQLFormat(this DateTime? date)
        {
            return date?.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string ToShortDate(this DateTime date)
        {
            return date.ToString("MM/dd/yyyy");
        }
        
        public static string ToShortTime(this DateTime date)
        {
            return date.ToString("h:mm tt");
        }
    }
}