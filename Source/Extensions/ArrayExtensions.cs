﻿using System;

namespace Rair.Utilities.Core.Extensions
{
    public static class ArrayExtensions
    {
        public static string GetString(this byte[] bytes)
        {
            var chars = new char[bytes.Length / sizeof(char)];
            Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
