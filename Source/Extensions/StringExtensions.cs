﻿using System;

namespace Rair.Utilities.Core.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Returns true if value is empty string
        /// </summary>
        public static bool IsEmpty(this string value)
        {
            if (value == null) { return true; }
            if (value == "") { return true; }
            if (value == string.Empty) { return true; }

            return false;
        }

        /// <summary>
        /// Returns true if value is not empty
        /// </summary>
        public static bool HasContent(this string value)
        {
            if (value == null) return false;
            return !value.IsEmpty();
        }

        public static string LastChar(this string value)
        {
            return value.IsEmpty() ? "" : value.Substring(value.Length-1);
        }

        public static byte[] GetBytes(this string str)
        {
            var bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static bool EqualsIc(this string val, string equals)
        {
            return val.Equals(equals, StringComparison.CurrentCultureIgnoreCase);
        }


        public static int? ToInt(this string s)
        {
            var intOut = 0;
            if (int.TryParse(s, out intOut))
            {
                return intOut;
            }
            return null;
        }
    }
}