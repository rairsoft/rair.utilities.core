﻿using System;

namespace Rair.Utilities.Core.Utilities.Disposable
{
    /// <summary>
    /// Disposable object that performs an Action in Constructor and Dispose methods.
    /// </summary>
    public class Provisional : IDisposable
    {
        private readonly Action _startAction;
        private readonly Action _endAction;

        public Provisional(Action startAction, Action endAction, bool startTheAction = true)
        {
            _startAction = startAction;
            _endAction = endAction;

            if (startTheAction) StartTheAction();
        }

        public Provisional StartTheAction()
        {
            _startAction?.Invoke();
            return this;
        }

        public void Dispose()
        {
            _endAction?.Invoke();
        }
    }
}
