﻿using System;
using System.Collections.Generic;

namespace Rair.Utilities.Core.Utilities
{
    public enum ChangeType { Add, Remove, Clear }

    /// <summary>
    /// Inherits from Dictionary. Calls its own DictionaryChanged event in Add, Remove, and Clear.
    /// If event implemented then each change event will fire DictionaryChanged and report the type of change.
    /// </summary>
    public class ObservabledDictionary<TKey, TValue> : Dictionary<TKey, TValue>
    {
        public event EventHandler DictionaryChanged;
        
        public new void Add(TKey key, TValue value)
        {
            DictionaryChanged?.Invoke(this, new DictionaryChangedEventArgs( ChangeType.Add));
            base.Add(key, value);
        }

        public new void Remove(TKey key)
        {
            DictionaryChanged?.Invoke(this, new DictionaryChangedEventArgs(ChangeType.Remove));
            base.Remove(key);
        }

        public new void Clear()
        {
            DictionaryChanged?.Invoke(this, new DictionaryChangedEventArgs(ChangeType.Clear));
            base.Clear();
        }
    }

    public class DictionaryChangedEventArgs : EventArgs
    {
        public ChangeType Change {get;}

        public DictionaryChangedEventArgs(ChangeType changeType )
        {
            Change = changeType;
        }
    }
}
