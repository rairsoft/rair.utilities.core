﻿using System.Linq;
using System.Reflection;
using Rair.Utilities.Core.Extensions;

namespace Rair.Utilities.Core.Utilities
{
    public static class CopyUtilities
    {
        /// <summary>
        /// Clones matching properties between two objects.  
        /// Only validates each property sets can be retrieved for each object.
        /// Does not validates objects are of the same type; simply looks for 
        /// matching properties and copies data from source to target.
        /// </summary>
        /// <returns>Result.Success if copy is attempted.</returns>
        public static Result<TTarget> FuzzyClone<TSource, TTarget>(TSource source, ref TTarget target)
        {
            if (source == null) return Result<TTarget>.Failure<TTarget>("Source cannot be null");

            var targetProps = typeof (TTarget).GetRuntimeProperties().ToList();
            var sourceProps = typeof (TSource).GetRuntimeProperties().ToList();

            if (sourceProps.IsEmpty() || targetProps.IsEmpty()) return Result<TTarget>.Failure<TTarget>("No runtime properties found.");


            foreach (var targetProp in targetProps)
            {
                var sourceProp = sourceProps.FirstOrDefault(sp => sp.Name.Equals(targetProp.Name) && sp.PropertyType == targetProp.PropertyType);
                if (sourceProp == null) continue;

                targetProp.SetValue(target, sourceProp.GetValue(source));
            }

            return Result<TTarget>.Success<TTarget>().SetReturn(target);
        }

        /// <summary>
        /// Validates source and target are of the same type.
        /// Does not validate property count.  If property from source is found
        /// on target then the data is copied else it is ignored.
        /// </summary>
        /// <returns>Result.Success if copy is attempted.</returns>
        public static Result DirectClone<TSource, TTarget>(TSource source, ref TTarget target)
        {
            if (source == null) return Result.Failure("Source cannot be null");

            var sourceT = typeof (TSource);
            var targetT = typeof(TTarget);

            if (sourceT != targetT) return Result.Failure("Types do not match");

            return FuzzyClone(source, ref target);
;        }
    }
}
