﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Rair.Utilities.Core
{
    public enum ResultType { Success, Failure, Nothing, Found, Missing }

    public class Result
    {
        public Exception Exception { get; internal set; }

        public ResultType Type { get; internal set; }

        public string Message { get; internal set; }

        public bool IsSuccess => Type == ResultType.Success;
        public bool IsFailure => Type == ResultType.Failure;
        public bool IsNothing => Type == ResultType.Nothing;
        public bool IsFound => Type == ResultType.Found;

        public static Result Success(string message = null)
        {
            var result = new Result { Type = ResultType.Success, Message = message };
            return result;
        }

        public static Result Failure(string message = null, Exception exception = null)
        {
            var result = new Result { Type = ResultType.Failure, Message = message, Exception = exception};
            return result;
        }

        public static Result Nothing(string message = null)
        {
            var result = new Result { Type = ResultType.Nothing, Message = message };
            return result;
        }

        public override string ToString()
        {
            return Type.ToString();
        }
    }

    public class Result<T> : Result
    {
        public T ReturnValue { get; private set; }

        public static Result<T> Success<T>(string message = null)
        {
            var result = new Result<T> {Type = ResultType.Success, Message = message};
            return result;
        }

        public static Result<T> Failure<T>(string message = null, Exception exception = null)
        {
            var result = new Result<T>
            {
                Type = ResultType.Failure,
                Message = message,
                Exception = exception
            };
            return result;
        }

        public static Result<T> Nothing<T>(string message = null)
        {
            var result = new Result<T> { Type = ResultType.Nothing, Message = message };
            return result;
        }

        public Result<T> SetReturn(T t)
        {
            ReturnValue = t;
            return this;
        }
    }
}