﻿using System;

namespace Rair.Utilities.Core
{
    public class ProgramInfo
    {
        public DateTime BuildDate {get;set;}
        public int Major {get;set;}
        public int Minor {get;set; }
        public int Build { get; set; }
        public int Revision { get; set; }

        public override string ToString()
        {
            return this.Version();
        }
    }

    public static class ProgramInfoExtensions
    {
        public static string Version(this ProgramInfo info, bool includeRev=false)
        {
            var rev = $"{info.Major}.{info.Minor}.{info.Build}";

            if (includeRev) rev += $"{info.Revision}";

            return rev;
        }
    }
}
