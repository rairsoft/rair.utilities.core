[![Build status](https://ci.appveyor.com/api/projects/status/vfulo55kpu9e2svf/branch/master?svg=true)](https://ci.appveyor.com/project/blairacuda/rair-utilities-core/branch/master)  [![NuGet Version](http://img.shields.io/nuget/v/Rair.Utilities.Core.svg?style=flat)](https://www.nuget.org/packages/Rair.Utilities.Core/)

# README #

Portable Class Library with some C# helper methods.  There are a handful of extension methods that we feel are missing from the out of the box C# library.  This is a growing collection of helpers that we have built and will continue building and providing for anyone to use.

### What is this repository for? ###

* PCL helper methods
* Valid for .Net 4.5, ASP.NET Core 1.0, Windows 8, Windows Phone 8.1, Xamarin.Android, and Xamarin.iOS