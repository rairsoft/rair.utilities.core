﻿using Rair.Utilities.Core.Tests.Mocks;
using Rair.Utilities.Core.Utilities;
using Xunit;

namespace Rair.Utilities.Core.Tests.Utilities
{
    public class CopyUtilitiesTests
    {
        [Fact()]
        public void When_try_fuzzy_clone_with_valid_objects_return_true()
        {
            var source = new Student();
            var target = new Student();

            var res = CopyUtilities.FuzzyClone(source, ref target);
            Assert.Equal(true, res.IsSuccess);
        }

        [Fact()]
        public void When_try_fuzzy_clone_with_null_source_return_false()
        {
            var target = new Student();

            var res = CopyUtilities.FuzzyClone<Student, Student>(null, ref target);
            Assert.Equal(true, res.IsFailure);
        }

        [Fact()]
        public void When_try_direct_clone_with_same_types_return_true()
        {
            var source = new Student();
            var target = new Student();

            var res = CopyUtilities.DirectClone(source, ref target);
            Assert.Equal(true, res.IsSuccess);
        }

        [Fact()]
        public void When_try_direct_clone_with_diff_types_return_false()
        {
            var source = new Student();
            var target = new TextBook();

            var res = CopyUtilities.DirectClone(source, ref target);
            Assert.Equal(true, res.IsFailure);
        }

        [Fact()]
        public void When_try_direct_clone_with_null_source_return_false()
        {
            var target = new TextBook();

            var res = CopyUtilities.DirectClone<Student, TextBook>(null, ref target);
            Assert.Equal(true, res.IsFailure);
        }
    }
}
