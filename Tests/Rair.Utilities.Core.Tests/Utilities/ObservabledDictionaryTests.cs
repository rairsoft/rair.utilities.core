﻿using System;
using Rair.Utilities.Core.Tests.Mocks;
using Rair.Utilities.Core.Utilities;
using Xunit;

namespace Rair.Utilities.Core.Tests.Utilities
{
    public class ObservabledDictionaryTests
    {
        [Fact()]
        public void When_remove_existing_key_return_true()
        {
            try
            {
                var mock = CreateMockObject();
                mock.Remove(0);
                Assert.True(true);
            }
            catch (Exception ex)
            {
                Assert.False(false, ex.ToString());
            }
        }

        [Fact()]
        public void When_remove_missing_key_return_false()
        {
            try
            {
                var mock = CreateMockObject();
                mock.Remove(99);
                Assert.False(false);
            }
            catch (Exception ex)
            {
                Assert.True(true, ex.ToString());
            }
        }

        [Fact()]
        public void When_add_new_key_return_true()
        {
            try
            {
                var mock = CreateMockObject(withData: false);
                mock.Add(0, StudentFactory.CreateStudent1());
                Assert.True(true);
            }
            catch (Exception ex)
            {
                Assert.False(false, ex.ToString());
            }
        }

        [Fact()]
        public void When_add_duplicate_key_return_false()
        {
            try
            {
                var mock = CreateMockObject();
                mock.Add(0, StudentFactory.CreateStudent2());
                Assert.False(false);
            }
            catch (Exception ex)
            {
                Assert.True(true, ex.ToString());
            }
        }

        [Fact()]
        public void When_clear_succeeds_return_true()
        {
            try
            {
                var mock = CreateMockObject();
                mock.Clear();
                Assert.True(true);
            }
            catch (Exception ex)
            {
                Assert.False(false, ex.ToString());
            }
        }

        private static ObservabledDictionary<int, Student> CreateMockObject(bool withData = true)
        {
            var mock = new ObservabledDictionary<int, Student>();
            if (!withData) return mock;

            var students = StudentFactory.CreateStudentList();
            foreach (var student in students)
            {
                mock.Add(student.Id, student);
            }
            return mock;
        }
    }
}
