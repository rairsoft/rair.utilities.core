﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rair.Utilities.Core.Extensions;
using Xunit;

namespace Rair.Utilities.Core.Tests.Extensions
{
    public class StringExtensionsTests
    {
        [Fact()]
        public void When_isempty_called_on_null_string_return_true()
        {
            string mock = null;
            Assert.Equal(true, StringExtensions.IsEmpty(mock));
        }

        [Fact()]
        public void When_isempty_called_on_blank_string_return_true()
        {
            var mock = "";
            Assert.Equal(true, mock.IsEmpty());
        }

        [Fact()]
        public void When_isempty_called_on_empty_string_return_true()
        {
            var mock = string.Empty;
            Assert.Equal(true, mock.IsEmpty());
        }

        [Fact()]
        public void When_isempty_called_on_nonempty_string_return_false()
        {
            var mock = "test string";
            Assert.Equal(false, mock.IsEmpty());
        }

        [Fact()]
        public void When_hascontent_called_on_null_string_return_false()
        {
            string mock = null;
            Assert.Equal(false, StringExtensions.HasContent(mock));
        }

        [Fact()]
        public void When_hascontent_called_on_blank_string_return_false()
        {
            var mock = "";
            Assert.Equal(false, mock.HasContent());
        }

        [Fact()]
        public void When_hascontent_called_on_empty_string_return_false()
        {
            var mock = string.Empty;
            Assert.Equal(false, mock.HasContent());
        }

        [Fact()]
        public void When_hascontent_called_on_nonempty_string_return_true()
        {
            var mock = "test string";
            Assert.Equal(true, mock.HasContent());
        }
    }
}
