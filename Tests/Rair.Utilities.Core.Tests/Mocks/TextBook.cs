﻿using System.Collections.Generic;

namespace Rair.Utilities.Core.Tests.Mocks
{
    public class TextBook
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public int CopyrightYear { get; set; }
    }

    public static class TextBookFactory
    {
        public static IEnumerable<TextBook> CreateTextBooks()
        {
            return new List<TextBook>
            {
                CreateTextBook1(),
                CreateTextBook2(),
                CreateTextBook3()
            };
        }

        public static TextBook CreateTextBook1()
        {
            return new TextBook
            {
                Title = "Aerodynamics and You",
                Author = "Mr. Aero Science",
                Publisher = "We R Books",
                CopyrightYear = 1986
            };
        }

        public static TextBook CreateTextBook2()
        {
            return new TextBook
            {
                Title = "How to Make Kitten Mittens",
                Author = "Charly Day",
                Publisher = "Philly Pubs and Stuff",
                CopyrightYear = 2011
            };
        }

        public static TextBook CreateTextBook3()
        {
            return new TextBook
            {
                Title = "Baseball of the Future",
                Author = "Jon Daniels",
                Publisher = "Ranger Publishing",
                CopyrightYear = 2021
            };
        }
    }
}
