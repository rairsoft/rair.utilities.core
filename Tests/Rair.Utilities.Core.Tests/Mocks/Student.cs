﻿using System.Collections.Generic;

namespace Rair.Utilities.Core.Tests.Mocks
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int Age { get; set; }
        public double Score { get; set; }
    }

    public static class StudentFactory
    {
        public static IEnumerable<Student> CreateStudentList()
        {
            return new List<Student>
            {
                CreateStudent1(),
                CreateStudent2(),
                CreateStudent3(),
                CreateStudent4()
            };
        }

        public static Student CreateStudent1()
        {
            return new Student()
            {
                Id = 0,
                Name = "Chris Blair",
                Address = "123 East St. Austin, TX",
                Phone = "012.345.6789",
                Age = 32,
                Score = 97.5
            };
        }

        public static Student CreateStudent2()
        {
            return new Student()
            {
                Id = 1,
                Name = "Jordan Blair",
                Address = "123 East St. Austin, TX",
                Phone = "987.654.3210",
                Age = 29,
                Score = 95
            };
        }

        public static Student CreateStudent3()
        {
            return new Student()
            {
                Id = 2,
                Name = "Janna Banana",
                Address = "9444 Boom St Apt 3, Portland, OR",
                Phone = "649.236.1574",
                Age = 17,
                Score = 56.79
            };
        }

        public static Student CreateStudent4()
        {
            return new Student()
            {
                Id = 3,
                Name = "Shirley Girley",
                Address = "654 Uptown Dr. New York, New York",
                Phone = "958.316.7745",
                Age = 13,
                Score = 45.8
            };
        }
    }
}
